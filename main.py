from tkinter import *
import MySQLdb
from tkinter import messagebox
import sys

#Variables used further in the program
database = 'DVDSTORE'
list = ['DRAMA','HORROR','COMEDY','ROMANCE','ACTION','OTHERS']
struct = "CREATE TABLE IF NOT EXISTS DVDINFO(" \
         "ID INT AUTO_INCREMENT PRIMARY KEY," \
         "TITLE VARCHAR(20) NOT NULL," \
         "STAR_NAME CHAR(20)," \
         "YOR VARCHAR(4)," \
         "GENRE ENUM('DRAMA','HORROR','COMEDY','ROMANCE','ACTION','OTHERS')" \
         ");"


# create a database in mysql
db = MySQLdb.connect("localhost", "root", "password")

cursor = db.cursor()
cursor.execute("SET sql_notes = 0; ")
cursor.execute("CREATE DATABASE IF NOT EXISTS DVDSTORE;")
"""
try:
    cursor.execute("CREATE DATABASE IF NOT EXISTS DVDSTORE;")
except Exception as e:
    pass
print "Database is created..."
"""
db.close()

db = MySQLdb.connect("localhost", "root", "password",database)
cursor = db.cursor()

"""
try:
    cursor.execute("DROP TABLE IF EXISTS DVDINFO;")
except Exception as e:
    print str(e)
"""
cursor.execute("SET sql_notes = 0; ")
cursor.execute(struct)


class Application(Frame, object):
    def __init__(self, master):
        super(Application, self).__init__(master)
        self.grid()
        self.DVD_menu()

    def DVD_menu(self):
        self.b1 = Button(self,text="ADD DVD",command=self.onClick_add)
        self.b1.grid(row=1,column=3)
        self.b1.pack(fill=X)

        self.b2 = Button(self,text="SEARCH",command=self.onClick_search)
        self.b2.grid(row=3,column=3)
        self.b2.pack(fill=X)

        self.b3 = Button(self,text="MODIFY DVD",command=self.onClick_modify)
        self.b3.grid(row=5,column=3)
        self.b3.pack(fill=X)

        self.b4 = Button(self,text="DELETE DVD",command=self.onClick_delete)
        self.b4.grid(row=7,column=3)
        self.b4.pack(fill=X)

        self.b5 = Button(self, text="EXIT", command=self.onClick_exit)
        self.b5.grid(row=9, column=3)
        self.b5.pack(fill=X)

    def onClick_add(self):
        self.top = Toplevel()
        self.top.title("ADD DVD")
        self.top.geometry("400x300")
        self.top.transient(self)
        self.app = ADD(self.top)

    def onClick_search(self):
        self.top = Toplevel()
        self.top.title("SEARCH DVD")
        self.top.geometry("400x300")
        self.top.transient(self)
        self.app = SRCH(self.top)

    def onClick_modify(self):
        self.top = Toplevel()
        self.top.title("MODIFY DVD")
        self.top.geometry("400x300")
        self.top.transient(self)
        self.app = MODIFY(self.top)

    def onClick_delete(self):
        self.top = Toplevel()
        self.top.title("DELETE DVD")
        self.top.geometry("400x300")
        self.top.transient(self)
        self.app = DELETE(self.top)

    def onClick_exit(self):
        root.destroy()

class ADD(object):
    def __init__(self,master):
        self.master = master
        self.frame = Frame(self.master)
        self.widgets()

    def widgets(self):
        self.l1 = Label(self.master,text="Title")
        self.l1.pack()
        self.l1.grid(row=1,column=1)
        self.e1 = Entry(self.master)
        self.e1.grid(row=1,column=2)

        self.l2 = Label(self.master,text="Star name")
        self.l2.pack()
        self.l2.grid(row=2, column=1)
        self.e2 = Entry(self.master)
        self.e2.grid(row=2, column=2)

        self.l3 = Label(self.master, text="Year of Release")
        self.l3.pack()
        self.l3.grid(row=3, column=1)
        self.e3 = Entry(self.master)
        self.e3.grid(row=3, column=2)

        self.l4 = Label(self.master, text="Genre")
        self.l4.pack()
        self.l4.grid(row=4, column=1)
        self.e4 = Entry(self.master)
        self.e4.grid(row=4, column=2)

        self.b1 = Button(self.master,text="Submit",command=self.onSubmit)
        self.b1.grid(row=5, column=2)

    def onSubmit(self):
        flag = True
        title = self.e1.get().upper()
        star = self.e2.get().upper()
        yor = self.e3.get().upper()
        genre = self.e4.get().upper()

        title = title.strip(" ")
        star = star.strip(" ")
        yor = yor.strip(" ")
        genre = genre.strip(" ")

        if not title:
            messagebox.showinfo("Information","Title can't be empty")
            flag = False

        temp_sql = "SELECT * FROM DVDINFO WHERE TITLE = '%s'" % title
        cursor.execute(temp_sql)
        results = cursor.fetchall()

        if results:
            for item in results:
                if title in item:
                    messagebox.showwarning("Warning","A DVD record with the same title is present")
                    flag = False

        if not star:
            messagebox.showinfo("Information","Star name can't be empty")
            flag = False

        if not yor:
            messagebox.showinfo("Information","Year of release can't be empty")
            flag = False

        if not genre:
            messagebox.showinfo("Information","Genre can't be empty")
            flag = False

        elif genre not in list:
            messagebox.showinfo("Information","Select genre from list provided: DRAMA,HORROR,COMEDY,ROMANCE,ACTION,OTHERS")
            flag = False

        if flag == True:
            sql = "INSERT INTO DVDINFO(TITLE,STAR_NAME,YOR,GENRE) VALUES('%s','%s','%s','%s')" % (title, star, yor, genre)
            try:
                cursor.execute(sql)
                db.commit()
                messagebox.showinfo("Information","New DVD record added in database")
            except Exception as e:
                db.rollback()


class SRCH(object):
    def __init__(self,master):
        self.master = master
        self.frame = Frame(self.master)
        self.widgets()

    def widgets(self):
        self.l1 = Label(self.master, text="Title")
        self.l1.pack()
        self.l1.grid(row=1, column=1)
        self.e1 = Entry(self.master)
        self.e1.grid(row=1, column=2)

        self.l2 = Label(self.master, text="Star name")
        self.l2.pack()
        self.l2.grid(row=2, column=1)
        self.e2 = Entry(self.master)
        self.e2.grid(row=2, column=2)

        self.l3 = Label(self.master, text="Year of Release")
        self.l3.pack()
        self.l3.grid(row=3, column=1)
        self.e3 = Entry(self.master)
        self.e3.grid(row=3, column=2)

        self.l4 = Label(self.master, text="Genre")
        self.l4.pack()
        self.l4.grid(row=4, column=1)

        variable = StringVar(self.master)
        variable.set("Select genre")  # default value

        self.p1 = OptionMenu(self.master, variable, 'DRAMA','HORROR','COMEDY','ROMANCE','ACTION','OTHERS')
        #self.e4 = Entry(self.master)
        self.p1.grid(row=4, column=2)

        self.e4 = variable

        self.b1 = Button(self.master, text="Submit", command=self.new)
        self.b1.grid(row=5, column=2)

    def new(self):
        self.top = Toplevel()
        self.top.title("Search results")
        self.top.geometry("500x400")
        self.app = onSubmit(self.top,self.e1,self.e2,self.e3,self.e4)

class onSubmit(object):
    def __init__(self,master,e1,e2,e3,e4):
        self.e1 = e1
        self.e2 = e2
        self.e3 = e3
        self.e4 = e4

        self.master = master
        self.frame = Frame(self.master)
        self.widget()

    def widget(self):
        temp = []
        adj = []
        cont = []

        title = self.e1.get().upper()
        star = self.e2.get().upper()
        yor = self.e3.get().upper()
        genre = self.e4.get()

        if genre == "Select genre":
            genre = ""

        title = title.strip(" ")
        star = star.strip(" ")
        yor = yor.strip(" ")
        genre = genre.strip(" ")

        sql = "SELECT * FROM DVDINFO"
        cursor.execute(sql)
        results = cursor.fetchall()

        if title:
           for item in results:
               if title in item:
                   cont.append(item)


        if star:
            if cont:
                for item in cont:
                    if star in cont:
                        temp.append(item)
            if not cont:
                for item in results:
                    if star in item:
                        cont.append(item)

            if temp:
                cont = temp
                temp = []
            elif not temp:
                cont = []


        if yor:
            if cont:
                for item in cont:
                    if yor in cont:
                        temp.append(item)
            if not cont:
                for item in results:
                    if yor in item:
                        cont.append(item)

            if temp:
                cont = temp
                temp = []
            elif not temp:
                cont = []


        if genre:
            if cont:
                for item in cont:
                    if genre in item:
                        temp.append(item)
            if not cont:
                for item in results:
                    if genre in item:
                        cont.append(item)

            if temp:
                cont = temp
                temp = []
            elif not temp:
                cont = []

        if cont:
            i = 0
            length = len(cont)
            while i <= length-1:
                self.i = Label(self.master, text="Title = %s, star = %s, year of release = %s, genre = %s\n"%(cont[i][1],cont[i][2],cont[i][3],cont[i][4]))
                self.i.pack()
                self.i.grid(row=i, column=1)
                i+=1

        elif not cont:
            self.l1 = Label(self.master, text="No DVD records suffice your requirement.")
            self.l1.pack()
            self.l1.grid(row=1, column=1)

class MODIFY(object):
    def __init__(self,master):
        self.master = master
        self.frame = Frame(self.master)
        self.widgets()

    def widgets(self):
        self.l1 = Label(self.master, text="Title")
        self.l1.pack()
        self.l1.grid(row=1, column=1)
        self.e1 = Entry(self.master)
        self.e1.grid(row=1, column=2)

        self.l2 = Label(self.master, text="Star name")
        self.l2.pack()
        self.l2.grid(row=2, column=1)
        self.e2 = Entry(self.master)
        self.e2.grid(row=2, column=2)

        self.l3 = Label(self.master, text="Year of Release")
        self.l3.pack()
        self.l3.grid(row=3, column=1)
        self.e3 = Entry(self.master)
        self.e3.grid(row=3, column=2)

        self.l4 = Label(self.master, text="Genre")
        self.l4.pack()
        self.l4.grid(row=4, column=1)
        self.e4 = Entry(self.master)
        self.e4.grid(row=4, column=2)

        self.b1 = Button(self.master, text="Submit", command=self.onSubmit)
        self.b1.grid(row=5, column=2)

    def onSubmit(self):
        flag = True
        title = self.e1.get().upper()
        star = self.e2.get().upper()
        yor = self.e3.get().upper()
        genre = self.e4.get().upper()

        title = title.strip(" ")
        star = star.strip(" ")
        yor = yor.strip(" ")
        genre = genre.strip(" ")

        if not title:
            messagebox.showinfo("Information", "Title can't be empty.")
            flag = False

        temp_sql = "SELECT * FROM DVDINFO WHERE TITLE = '%s'" % title
        cursor.execute(temp_sql)
        results = cursor.fetchall()

        for item in results:
            if title in item:
                flag = True

        if not star:
            messagebox.showinfo("Information", "Star name can't be empty.")
            flag = False

        if not yor:
            messagebox.showinfo("Information", "Year of release can't be empty.")
            flag = False

        if not genre:
            messagebox.showinfo("Information", "Genre can't be empty.")
            flag = False

        elif genre not in list:
            messagebox.showinfo("Information",
                                "Select genre from list provided: DRAMA,HORROR,COMEDY,ROMANCE,ACTION,OTHERS.")
            flag = False

        if flag == True:
            sql = "UPDATE DVDINFO SET STAR_NAME = '%s', YOR = '%s', GENRE = '%s' WHERE TITLE = '%s'" % (
            star, yor, genre, title)
            try:
                cursor.execute(sql)
                db.commit()
                messagebox.showinfo("Information","DVD record successfully modified.")
            except Exception as e:
                print str(e)
                db.rollback()

        else:
            messagebox.showinfo("Information", "DVD record is not available.")


class DELETE(object):
    def __init__(self,master):
        self.master = master
        self.frame = Frame(self.master)
        self.widgets()

    def widgets(self):
        self.l1 = Label(self.master, text="Title")
        self.l1.pack()
        self.l1.grid(row=1, column=1)
        self.e1 = Entry(self.master)
        self.e1.grid(row=1, column=2)

        self.b1 = Button(self.master, text="Submit", command=self.onSubmit)
        self.b1.grid(row=5, column=2)

    def onSubmit(self):
        title = self.e1.get().upper()

        temp_sql = "SELECT * FROM DVDINFO WHERE TITLE = '%s'" % title
        cursor.execute(temp_sql)
        results = cursor.fetchall()

        if results:
            for item in results:
                if title not in item:
                    messagebox.showwarning("Warning","No DVD available with that title.")

            sql = "DELETE FROM DVDINFO WHERE TITLE = '%s'" % title
            try:
                cursor.execute(sql)
                db.commit()
                messagebox.showinfo("Information","DVD record is successfully deleted.")
            except Exception as e:
                db.rollback()
                messagebox.showerror("Error","Problem deleting record.")

        elif not results:
            messagebox.showwarning("Warning","Sorry, DVD is not available.")


root = Tk()
root.geometry("400x300")
app = Application(root)
app.mainloop()
